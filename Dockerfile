FROM mhart/alpine-node:9

RUN apk add --update git && \
  rm -rf /tmp/* /var/cache/apk/*

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
RUN git clone https://gitlab.com/Alvaro.z/digital_fingerprinting.git /usr/src/app
RUN npm install

# Bundle app source
COPY . /usr/src/app

CMD [ "npm", "start" ]