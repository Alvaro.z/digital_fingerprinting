var express = require('express');
var fingerprint = require('../Service/Schema/fingerprint');
var results = require('../Service/Schema/results');
var google = require('../Service/Schema/google');
var request = require('request-promise');
var router = express.Router();
var requestIp = require('request-ip');
var comparison = require('../Service/comparison');
var util = require('../Service/util');
var auth = require('../Service/auth');

/**
 * Route zum Abrufen aller Suchanfragen
 */
router.get('/results', function (req, res, next) {

  console.log("Route data/results".yellow);

  if (req.cookies.ID) {
    auth.checkLogin(req.cookies.ID)
      .then(result => {
        if (result) {
          results.find({})
            .then(result => {
              console.log("Find results successful".green);
              res.send(result);
            })
            .catch(error => {
              console.log("No results are found".red);
              console.log(error);
              res.error("Nothing found");
            })
        } else {
          res.redirect("/");
        }
      })
      .catch(error => {
        console.log("Check Login /results failed".red);
        res.send("Fehler bei der Ausgabe der Results");
      })
  } else {
    res.redirect("/");
  }

});

/**
 * Route zum Abrufen aller Fingerabdrücke
 */
router.get('/fingerprint', function (req, res, next) {

  console.log("Route data/fingerprint".yellow);

  if (req.cookies.ID) {
    auth.checkLogin(req.cookies.ID)
      .then(result => {
        if (result) {
          fingerprint.find({})
            .then(result => {
              console.log("Find fingerprints successful".green);
              res.send(result);
            })
            .catch(error => {
              console.log("No fingerprints are found".red);
              console.log(error);
              res.error("Nothing found");
            })
        } else {
          res.redirect("/");
        }
      })
      .catch(error => {
        console.log("Check Login /fingerprint failed".red);
        res.send("Fehler bei der Ausgabe der Fingerprints");
      })
  } else {
    res.redirect("/");
  }

});

/**
 * Route zum Abrufen aller Keys angemeldeter Nutzer
 */
router.get('/google', function (req, res, next) {

  console.log("Route data/google".yellow);

  if (req.cookies.ID) {
    auth.checkLogin(req.cookies.ID)
      .then(result => {
        if (result) {
          google.find({})
            .then(result => {
              console.log("Find Google successful".green);
              res.send(result);
            })
            .catch(error => {
              console.log("Nothing found".red);
              console.log(error);
              res.error("No google datas are found");
            })
        } else {
          res.redirect("/");
        }
      })
      .catch(error => {
        console.log("Check Login /results failed".red);
        res.send("Fehler bei der Ausgabe der Google Daten");
      })
  } else {
    res.redirect("/");
  }

});

/**
 * 
 * @description Asynchrone Funktionen zum Abrufen der Wahrscheinlichkeiten der Suchanfragen und Sortierung dieser
 * @param {Object} req Request Objekt
 * @returns {Obejct[]} Array der Form [{search|prob}]
 * @requires Nodejs v8 or higher
 */
async function a(req) {
  var r = [];
  r = await comparison(req.body);
  //console.log("++++++++++++++++ Unsortiert +++++++++++++++++++++++++".rainbow);
  //console.log(r);
  r = util.sort(r);
  r = util.shortenNumber(r, 5);
  //console.log("++++++++++++++++ Sortiert +++++++++++++++++++++++++".rainbow);
  //console.log(r);
  return r;
}
router.post('/', function (req, res, next) {

  console.log("Route POST data/".yellow);

  a(req).then(r => {
    res.send(r);
  });

});

module.exports = router;
