var express = require('express');
var fingerprint = require('../Service/Schema/fingerprint');
var results = require('../Service/Schema/results');
var google = require('../Service/Schema/google');
var router = express.Router();
var requestIp = require('request-ip');
var auth = require('../Service/auth');
var config = require('../config.json');

/**
 * Route zum Abrufen der Startseite
 */
router.get('/', function (req, res, next) {

  console.log("Route /".yellow);

  if (req.cookies.ID) {
    res.render("index", { layout: "main", URL : config.URI_Frontend, id: req.cookies.ID });
  } else {
    res.render("index", { layout: "main", URL : config.URI_Frontend });
  }
});

/**
 * Route zum Abrufen des Backends
 */
router.get('/backend', function (req, res, next) {

  console.log("Route index/backend".yellow);

  if (req.cookies.ID) {
    auth.checkLogin(req.cookies.ID)
      .then(result => {
        if (result) {
          google.findOne({ _id: req.cookies.ID })
            .then(u => {
              console.log("Get Profile from DB successfull".green);
              res.render("backend", { layout: "main", User: u });
            })
            .catch(error => {
              console.log("Get Profile from DB failed".red);
              res.send("Fehler");
            })
        } else {
          res.redirect("/");
        }

      })

  } else {
    res.redirect("/");
  }
});

/**
 * Post zur Google Suche
 */
router.post('/', function (req, res, next) {

  console.log("Route index/backend".yellow);

  //Abrufen des aktuellen Fingerabdrucks, Speichern zusammen mit Suchanfrage und Weiterleiten zu Google
  fingerprint.find({ customFingerprint: req.body.fingerprintID })
    .then(result => {

      results.insertMany({ fingerprint: result[0]._id, search: req.body.q })
        .then(result => {
          console.log("Insert successfull".green);
          res.redirect(config.searchPage + req.body.q);
        })
        .catch(error => {
          console.log("Insert failed: ".red);
          console.log(error);
          res.redirect("/");
        })
    })
    .catch(err => {
      console.log("Find FingerprintID failed".red);
      console.log(err);
      res.redirect("/");
    })
})

module.exports = router;
