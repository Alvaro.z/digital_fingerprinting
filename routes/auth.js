var express = require('express');
var request = require('request-promise');
var router = express.Router();
var google = require('../Service/Schema/google');
var auth = require('../Service/auth');

/**
 * Redirect Route zum Login über Google
 */
router.get("/", (req, res, next) => {

    console.log("Route auth/".yellow);

    auth.login(req)
        .then(result => {
            console.log("Login successfull".green);
            res.cookie("ID", result).redirect("/backend");
        })
        .catch(error => {
            console.log("Login failed".red);
            console.log(error);
            res.send("Fehler im Login")
        })
})

/**
 * Route zum Logout
 */
router.get("/logout", (req, res, next) => {

    console.log("Route auth/logout".yellow);

    auth.logout(req.cookies.ID)
        .then(result => {
            console.log("Logout successfull".green);
            res.clearCookie("ID").redirect("/");
        })
        .catch(error => {
            console.log("Logout failed".red);
            res.send("Fehler im Logout");

        })
})

module.exports = router;