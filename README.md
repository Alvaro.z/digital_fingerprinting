# Digital Fingerprint

## Entstehung
Dieses Applikation enstand aus der Vorlesung "Neue Konezte (SW)" im 6. Semester an der DHBW Mannheim.
In dieser Vorlesungen sollte wir uns mit aktuellen Programmierthemen auseinandersetzen und diese innerhalb eines Projektes umsetzen.

## Aufgabenstellung
Die Aufgabestellung bestand aus der Programmierung einer Applikation, welche den Webseitenbesuchers mithilfe eines "Digital Fingerprint" wiedererkennt.
Dabei soll die Applikation ohne ein Frontend lauffähig sein.
Zudem soll auf eine Identifikation mithilfe von User Management, Cookies, Sessions oder anderen Authentifizierungsmethoden verzichtet werden.
Bei der Applikation soll die Wiedererkennung mit einer möglichst hohe Erfolgsqoute garantiert und wenn möglich ein Maschine Learning Algorithmus verwendet werden.
Das Ergbnis soll nach Fertigstellung vorgestellt werden. Dabei soll nachgewiesen werden, dass die geforderten Funktionalitäten umgesetzt wurden.

### Anforderungen
Nicht-funktional:
- [x] Qualität des Codes
- [x] 12 Factors
- [x] Repository (inkl. README.md)
- [x] TLS
- [x] Dockerfile
- [x] OAuth
- [x] GoCD

Funktional:
- [x] Wiedererkennung eines Benutzer ohne Authentifizierungsmethoden
- [x] Unterschedung von Benutzern trotz gleicher IP-Adresse
- [x] Nachweis der Funktionalität
- [x] Machnine Learning Ansatz

### Hilfsmittel
- Server mit Betriebssystem Ubuntu von der DHBW Mannheim
- Technisches und fachliches Know-How unserer Dozenten
- Vorlesungsmaterial und Webinare 

## Funktionalitäten der Applikation
Die programmierte Applikatition beinhaltet in dieser Ausfertigung sowohl Backend- als auch Frontend-Funktionalitäten.
Sie ist komplett auf dem bereitgestellten Server der DHBW Mannheim  deployed.

### Backend
Das Backend setzt sich aus folgenden Funktionalitäten zusammen:
- Identifizierung des "Digital Fingerprint" vom Webseitenbesucher
- Suchfunktion, welche die Suchergebnisse zusammen mit dem "Digital Fingerprint" speichert
- Wiedererkenunung des "Digital Fingerprint" mit den dazugehöigen Suchergebnissen und deren Übereinstimmungswahrscheinlichkeiten 
- Maschine Learing Algorithmus Ansatz mithilfe von Wahrscheinlichkeitsberechnung
- Anbindung an eine NoSQL-Datenbank - MongoDB und Implementierung der benötigten Schemata der JSON-Objekte
- Abruffunktion der gespeicherten "Digital Fingerprints" und der Suchergbnisse
- Backendlogin mithilfe von OAuth Google-Login
- Einbindung eines TLS-Zertifikat mit automatischer Weiterleitung von http zu https

### Frontend
Das Frontend setzt sich aus folgenden Funktionalitäten zsuammen:
- Darstellung des "Digital Fingerprint" mit dazugehörigen Suchergebnissen und deren Übereinstimmungswahrscheinlichkeiten
- Suchfunktion mit Weiterleitung auf Google
- Backend-Seite zur Darstellung des angemeldeten Google-Nutzers
- Innerhalb der Backend-Seite besteht die Möglichkeit des Ausloggens und der Ausgabe der gespeicherten "Digital Fingerpints" und Suchergebnissen in Rohform

## Installation der Applikation Lokal
Die Applikation kann ganz einfach Lokal installiert werden. Hierzu muss lediglich das Repository heruntergeladen, die Applikation installiert und Anpassungen vorgenommen werden.
Im Folgenden werden die auszuführenden Schritte aufgezeigt:
1. Repository herunterladen und im Zielordner entpacken oder im Zielordner clonen
2. Im Zielordner eine bevorzugte Konsole öffnen wie bspw. [GitBash] (https://git-scm.com/downloads)
3. Den Konsolenbefehl ``npm install`` absetzen, um die Applikation zu installieren (Node Modules)
4. Die Applikation in einer bevorzugten Enticklungsumgebung öffnen wie bspw. in [Visual Studio Code] (https://code.visualstudio.com/)
5. In der Datei _config.json_ alle Variablen mit den bevorzugten befüllen (Bspw. Datenbank, Ports, Hardcoded IP-Adressen usw.)
6. Mit dem Konsolenbefehl ``npm start`` wird die Applikation gestartet mit einer verbindung zu der eingetragenen Datenbank

Die Applikation läuft Lokal standartmäßig auf [Localhost] (http://localhost) mit automatischer Weiterleitung von http auf https
Das Installieren auf einem Server mithilfe von [Docker] (https://www.docker.com/what-docker) wird im nächsten Abschnitt weiter erklärt.

## Installation der Applikation auf einem Server
Im Fall das die Applikation auf einem Server laufen soll gibt es in diesem Repository ein [Dockerfile] (https://gitlab.com/Alvaro.z/digital_fingerprinting/blob/master/Dockerfile) mit dem die Applikation komplett auf den Server installiert werden kann.
Für die Installation gibt es zwei Möglichkeiten welche im folgenden erklärt werden:

### Normale Installation
Damit die Installationa auf den Server ermöglicht werden kann muss auf diesem [Docker] (https://www.docker.com/what-docker) installiert sein und folgende Schritte ausgeführt werden:
1. Dockerfile auf den Server laden und im Zielordner auf dem Server speichern
2. Im Zielordner den Konsolenbefehl ``docker build --no-cache --tag fingerprint:latest --tag fingerprint:commit --label "org.label-schema.name=\"fingerprint\"" --file Dockerfile`` absetzen, um mithilfe des Dockerfiles ein Docker-Image zu erstellen
3. Des erstellte Docker-Image sollte mit dem Konsolenbefehl ``docker ps`` kontrolliert werden, ob dieses vollständig erstellt wurde
4. Nach Überprüfung kann die Applikation mithilfe des Images deployed werden. Hierzu gibt es zwei Möglichkeiten:
    - Man kann das Docker Image mit dem Konsolenbefehl ``docker run -d -p 33000:443 -p 43000:80 fingerprint`` deployen
    - Oder man kann, wenn [Docker Swarm] (https://docs.docker.com/engine/swarm/) auf dem Server eingerichtet ist, ein Docker Service mit dem Konsolenbefehl ``docker service create --name fingerprint --replicas 1 -p 33000:443 -p 43000:80 fingerprint:latest`` deployen

Hierbei ist zu beachten, dass bei Änderungen der Applikation der erstellte Docker Container mit ``docker rm (Container ID)`` und der Docker Service mit ``docker service rm fingerprint``gelöscht werden können.
Der Vorteil beim Service liegt in der eindeutigen Namensvergabe des Service gegebnüber der zufälligen Container ID vergabe, die lediglich mit ``docker ls`` herauszufinen ist.
Somit kann ein Service bei jedem Neustart mit dem gleichen Namen, der vergeben wurde, eindeutig gelöscht werden. Bei einem Container muss die ID erst noch herausgefundenw werden.
Nach erfolgreicher Löschung können die Schritte 2-4 erneut ausgeführt, um das aktualisierte Image neu zu deplyen.

### Installation mithilfe von GoCD
Wenn man nun den Anspruch hat, die Appikation automatisiert bei Änderungen auf den Server zu deployen, kann [GoCD] (https://www.gocd.org/) verwendet werden.
Für die Installation mithilfe von GoCD müssen folgende Schritte durchgeführt werden:
1. Man muss zunächst GoCD auf dem Server installieren.
2. Dann muss eine _Pipeline_ in GoCD erstellt werden, die mit einem Repository, in dem die Applikation liegt, verknüpft ist
3. In dieser _Pipeline_ muss nun eine _Stage_ für bspw. Docker erstellt werden
4. Innerhalb dieser _Stage_ werden die Befehle, die zum deployen benötigt werden, in einzelen _Jobs_ hinterlegt
3. Nachstehend die Befehle zur Erstellung einer _Stage_ und Deployen eines Docker Service
    - **buildimage**: ``docker build --no-cache --tag fingerprint:latest --tag fingerprint:commit --label org.label-schema.name="fingerprint" --file Dockerfile`` 
    - **removeOldService**: ``docker service rm fingerprint``
    - **createNewService**: ``docker service create --name fingerprint -p 33000:443 -p 43000:80 fingerprint:latest``

Nach erfolgreicher Umsetzung der Schritte kann die Pipeline gestartet werden. Dabei werden die _Jobs_ hintereinander aus der _Stage_ ausgeführt.
Das Ergebnis der Pipeline ist die installiete Applikation auf dem Server die sich nach jeder Änderung im Git-Repository von selbst neu depleyed.

### Aufruf der Applikation
Die Appliaktion lässt sich nun über die [http://IP Adresse:43000/] (http://141.72.16.156:43000/) _(Nur mit Verbindung zum VPN der DHBW-Mannheim möglich)_ erreichen mit automatischer Weiterleitung von http auf https.

## Maschine Learning Ansatz
Zum Thema Maschine Learning gab es in unserem Projekt Schwierigkeiten bzgl. des Algorithmus. 
Der Einzige der  nach ausführlicher Recherche in Frage kommt, war der [Clustering Algorithmus k-Means] (https://machinelearningmastery.com/a-tour-of-machine-learning-algorithms/).
Mit diesem könnte man in unseren Fall den identifizierten "Digital Finerprint" des Webseitenbesuchers durch clustern der einzelnen Fingerprinteigenschaften zu einem bestehenden zuordnen. 
D.h. der Algorithmus erstellt Cluster mit zusammengehörigen Daten, wie bspw. die Eigenschaften eines "Digital Fingerprints", die zusammen den "Digital Fingerprint" ergeben. 
Der Algotithmus clustert diese Daten solange bis es keine Möglichkeit mehr zum clustern gibt.
Das Ergbnis wären die gespeicherten "Digital Fingerprints" mit den genaue Eigenschaften, die nun mit dem identifizierten "Digital Finerprint" des Webseitenbesuchers abgleichen kann.
Somit könnte man mit einer hohen Wahrscheinlichkeit erfahren, ob es dem "Digital Fingerprint" schon gibt, er einem anderen ähnelt (einzelne Parameter wie der Browser differieren)) oder er einzigartig ist.

Dieser Algorithmus ist jedoch für unseren Anwendungsfall kompliziert, da ein "Digital Fignerprint" momentan 79 Eigenschaften besitzt. 
Im typischen [Beispiel] (http://blog.easysol.net/machine-learning-algorithms-3/) des k-Means Algorithmus mit der Blume gibt es lediglich 4 Eigenschaften.
Aus diesem Grund entschieden wir uns für einen Maschine Learning Ansatz der wie folgt aussieht:

Der Algorithmus nimmt sich den identifizierten "Digital Fingerprint", checkt diesen mit allen anderen vorhandendenen "Digital Fingerprints" und deren Eigenschaften ab und berechnet somit die Wahrscheinlchkeit der Übereinstimung zu den Suchergebnissen.
Im Anschluss werden die Suchergebnisse mit deren Wahrscheinlichkeit einer Übereinstimmung ausgegeben. Wenn die Wahrscheinlichkeit 100% beträgt dann wurden die Suchergebnisse über den identifizierten "Digital Fingerprint" abgesetzt.

## Zusammenfassend
Zusammenfassend lässt sich sagen, das wir eine Applikation entwickelt haben, die die oben beschriebenen Funktionalitäten und den oben beschriebenen Maschine Learing Ansatz beherrscht.
Wenn man nun die [Seite aus dem VPN der DHBW Mannheim] (http://141.72.16.156:43000/) aufruft, wird automatisch von HTTP auf HTTPS weitergeleitet. 
Es wird der "Digital Fingerprint" identifiziert und mit den voherigen Sucheingaben und deren Übereinstimmungswahrscheinlichkeit auf der Seite visuell ausgegeben.
Die Bestimmung der Wahrscheinlichkeiten wird mithilfe des Maschine Learning Ansatzes berechnet.
Alle Daten werden erhoben, d.h. "Digital Fingerprints", Suchergebnisse und Google-User/OAuth-Daten, werden in einer MongoDB auf dem Serer gespeichert.
Google-User/OAuth-Daten werden jedoch nur bis zum Logout gespeichert.Auf der Startseite befindet sich außerdem eine Sucheingabe, mit der man weitere Suchen über Google starten kann, welche beim erneuten Aufruf der Seiten in der Übersicht zu sehen sind.
Mithilfe einer OAuth von Google kann man sich mit seinem Google-Konto anmelden und gelangt somit ins Backend.
Im Backend sieht man den angemeldeten Google-User und kann mithilfe von Weiterleitungen sich die Suchergebnisse als auch die "Digital Fingerprints" in Rohfassung anschauen.
Durch eine Logout-Funktion wird man auf der Seite abgemeldet, jedoch nicht in Google, und wird auf die Startseite weitergeleitet.
Die vollständige Applikation wurde in dieses Repository geladen und sowohl mit einem [Dockerfile] (https://gitlab.com/Alvaro.z/digital_fingerprinting/blob/master/Dockerfile) als auch dieser [Readme] (https://gitlab.com/Alvaro.z/digital_fingerprinting/blob/master/README.md) ergänzt. 
Mithilfe des [Dockerfiles] (https://gitlab.com/Alvaro.z/digital_fingerprinting/blob/master/Dockerfile) und [GoCD] (http://141.72.16.156:8080/go/tab/pipeline/history/digitalFingerprint) wird die Applikation bei sämtlichen Änderungen automatisch auf den Server neu depleyed.

Die Seite ist über [http://141.72.16.156:43000/] (http://141.72.16.156:43000/) aufrufbar mit einer, wie schon erwähnt, automatischen Weiterleitung von HTTP auf HTTPS.

## Authors

* **Philipp Schreiner**  
* **Alvaro Zorn** 

## License
 **_GNU General Public License v3.0._**