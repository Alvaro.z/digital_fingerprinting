var express = require('express');
var request = require('request-promise');
var router = express.Router();
var google = require('../Service/Schema/google');
var config = require('../config.json');

/**
 * 
 * @param {Object} req Request Objekt
 * @returns {Number} Client ID
 * @description Führt login mit Google durch
 */
function login(req) {
    console.log("login()".yellow);
    return new Promise((resolve, reject) => {
        var auth_code = req.query.code,
            client_ID = config.client_ID,
            client_SECRET = config.client_SECRET,
            URI = config.URI_1 + auth_code + '&client_id=' + client_ID + '&client_secret=' + client_SECRET + config.URI_1_1;

        var options = {
            method: 'POST',
            uri: URI,
            json: true
        };
        //Tauschen des Auth Codes von Google in Access und Refresh Tokens
        request(options)
            .then(result => {
                console.log("Successful Login with Google".green);

                var access_TOKEN = result.access_token,
                    refresh_TOKEN = result.refresh_token,
                    expires_IN = result.expires_in,
                    token_TYPE = result.token_type;
                URI = config.URI_2 + access_TOKEN;
                options = {
                    method: 'GET',
                    uri: URI,
                    json: true
                }
                //Abrufen der Profilinformationen
                request(options)
                    .then(profile => {
                        console.log("Get Profile successful".green);

                        //Einfügen der Informationen in DB und Rückgabe der ID
                        google.insertMany({ access_token: access_TOKEN, refresh_token: refresh_TOKEN, Name: profile.name, Vorname: profile.given_name, Nachname: profile.family_name, Google_ID: profile.id, Foto: profile.picture, GPlus: profile.link })
                            .then(r => {
                                console.log("Insert Login Credentials successful".green);
                                console.log(r[0]._id);
                                resolve(r[0]._id);
                            })
                            .catch(e => {
                                console.log("Insert Login Credentials failed".red);
                                console.log(e);
                                reject(e);
                            })
                    })
                    .catch(e => {
                        console.log("Get Profile failed".red);
                        console.log(e);
                        reject(e);
                    })
            })
            .catch(error => {
                console.log("Login with Google failed".red);
                console.log(error);
                reject(error);
            })
    })
}

/**
 * 
 * @param {Object} id ID im Cookie
 * @returns {String} Erfolgreich oder Fehler
 * @description Funktion zum Logout eines angemeldeten Nutzers
 */
function logout(id) {

    console.log("logout()".yellow);

    return new Promise((resolve, reject) => {
        google.remove({ "_id": id })
            .then(result => {
                console.log("Delete profile successfull".green);
                resolve("Erfolgreich");
            })
            .catch(error => {
                console.log("Delete profile failed".red);
                console.log(error);
                reject("Fehler");
            })
    })
}

/**
 * 
 * @param {Object} id ID im Cookie
 * @returns {boolean} true oder false
 * @description Funktion zur Überprüfung des Login Status
 */
function checkLogin(id) {

    console.log("Check Login()".yellow);

    return new Promise((resolve, reject) => {
        google.findOne({ "_id": id })
            .then(result => {
                if (result != null) {
                    console.log("logged in".green);
                    resolve(true);
                } else {
                    console.log("not logged in".green);
                    resolve(false);
                }
            })
            .catch(error => {
                console.log("Fehler checkLogin()".red);
                console.log(error);
            })
    })
}
module.exports = { login, logout, checkLogin };