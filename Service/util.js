/**
  * @param {Object} res Array
  * @returns {Object} Sortiertes Array
  * @description Funktion zur Sortierung des Arrays Suchanfragen|Wahrscheinlichkeit nach Wahrscheinlichkeit absteigend
  */
function sort(res) {

    return res.sort(function (a, b) {
        if (a.prob > b.prob) {
            return -1;
        }
        else if (a.prob < b.prob) {
            return 1;
        } else return 0;
    });
}

 /**
  * @param {Object} res Array
  * @param {Object} n Anzahl der Stellen
  * @returns {Object} Formatiertes Array
  * @description Funktion zur Formatierung der Wahrscheinlichkeiten im Array Suchanfragen|Wahrscheinlichkeit
  */
function shortenNumber(res, n) {
    var temp = 0;
    for (var i = 0; i < res.length; i++) {
        temp = res[i].prob;
        temp = temp.toPrecision(n);
        res[i].prob = temp;
    }
    return res;
}

module.exports = { sort, shortenNumber };