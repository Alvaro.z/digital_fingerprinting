var fingerprints = require('../Service/Schema/fingerprint');
var results = require('../Service/Schema/results');

/**
 * @param {Object} fingerprint Vom Nutzer gesammelter Fingerprint
 * @returns {Object} Array aus Objekten der Form {search: %Suchanfrage%, prob: %Wahrscheinlichkeit%}
 * @description Funktionen zur Ermittlung der prozentualen Übereinstimmung mit den bisherigen Fingerabdrücken
 */
async function compare(fingerprint) {
    return new Promise((resolve, reject) => {
        console.log("Function Compare".yellow);
        var res = []; //Array zur Speicherung des Ergebnisses

        //Prüfung, ob Fingerabdruck bereits vorhanden ist
        fingerprints.findOne({ "customFingerprint": fingerprint.customFingerprint })
            .then(r => {
                console.log("Find custom Fingerprint successful".green);
                //Wenn Fingerabruck nicht vorhanden, einfügen in Datenbank
                if (r === null) {

                    fingerprints.insertMany(fingerprint)
                        .then(result => {
                            console.log("Insert fingerprint successfull".green);
                        })
                        .catch(error => {
                            console.log("Insert fingerprint failed".red);
                            console.log(error);
                        })


                }
                //Wenn vorhanden, Abfrage der bisherigen Suchanfragen dieses Abdrucks
                else {

                    results.find({ "fingerprint": r._id })
                        .then(rr => {
                            console.log("Find results successfull".green);
                            //Wenn Suchanfragen vorhanden, diese mit 100% Wahrscheinlichkeit in Ergebnisvariable einfügen
                            if (rr.length != 0) {
                                for (var n = 0; n < rr.length; n++) {
                                    res[n] = { "result": rr[n].search, "prob": 100 };
                                }
                            }
                        })
                        .catch(error => {
                            console.log("Error find Suchergebnisse".red)
                            reject(error);
                        })

                }

                //Abfragen aller Fingerabdrücke
                fingerprints.find({})
                    .then(result => {
                        console.log("Find all fingerprints".green);
                        resolve(work(res, result, fingerprint)); //Rückgabe: Ergbnis der Funktion Work
                    })
                    .catch(error => {
                        console.error("Error alle Fingerprints".red)
                        console.error(error);
                        reject(error);
                    })

            })
            .catch(e => {
                console.error("Error find custom fingerprint".red);
                console.error(e);
                reject(e);
            })
    })


}


 /**
  * @param {Object} res Variable mit bisherigen Ergebnissen
  * @param {Object} arr Variable mit bisherigen Ergebnissen
  * @param {Object} fingerprint aktueller Fingerabdruck
  * @returns {Object} versollständigtes Array aus Objekten der Form {search: %Suchanfrage%, prob: %Wahrscheinlichkeit%}
  * @description Funktion zur Berechnung der Wahrscheinlichkeit. Funktionsweise: Aus arr wird letztes Element geholt und aus arr gelöscht. Dieses wird mit fingerprint verglichen, prozentuale Übereinstimmung ermittelt und zusammen mit den bisherigen Suchanfragen zum res Objekt hinzugefügt und zurückgegeben. Dann Selbstaufruf von work so lange, bis alle Fingerabdrück verarbeitet sind.
  */
function work(res, arr, fingerprint) {
    return new Promise((resolve, reject) => {
        console.log("Work".green);
        console.log(arr.length);
        var tempArr = arr.pop(); //Holen und Löschen des letzten Elements aus arr
        var temp = 0; //temporäre Variable zur Speicherung der Anzahl der gleichen Eingeschaften der Fingerabdrücke

        //Wenn alle Fingerabdrücke verarbeitet werden, Rückgabe des unveränderten Arrays zur Beendigung der Rekursion
        if (tempArr === undefined) {
            console.log("Ende".yellow);
            resolve(res);
        }
        //Wennn noch Fingerabdrücke verarbeitet werden müssen
        else {
            //Abrufen der Suchanfragen des aktuell ausgewählten, bereits vorhandenem Fingerabdruck
            results.find({ "fingerprint": tempArr._id })
                .then(re => {

                    //Weiterverarbeitung nur, wenn bereits Anfragen zu diesem Abdruck vorhanden sind
                    if (re != null) {

                        console.log("Find custom fingerprint".green);

                        //Abgleich aller Eigenschaften
                        if (tempArr.userAgent == fingerprint.userAgent) {
                            temp++;
                        }
                        if (tempArr.userAgentLowerCase == fingerprint.userAgentLowerCase) {
                            temp++;
                        }
                        if (tempArr.browser == fingerprint.browser) {
                            temp++;
                        }
                        if (tempArr.browserVersion == fingerprint.browserVersion) {
                            temp++;
                        }
                        if (tempArr.browserMajorVersion == fingerprint.browserMajorVersion) {
                            temp++;
                        }
                        if (tempArr.isIE == fingerprint.isIE) {
                            temp++;
                        }
                        if (tempArr.isChrome == fingerprint.isChrome) {
                            temp++;
                        }
                        if (tempArr.isFirefox == fingerprint.isFirefox) {
                            temp++;
                        }
                        if (tempArr.isSafari == fingerprint.isSafari) {
                            temp++;
                        }
                        if (tempArr.isOpera == fingerprint.isOpera) {
                            temp++;
                        }
                        if (tempArr.isMobileSafari == fingerprint.isMobileSafari) {
                            temp++;
                        }
                        if (tempArr.engine == fingerprint.engine) {
                            temp++;
                        }
                        if (tempArr.engineVersion == fingerprint.engineVersion) {
                            temp++;
                        }
                        if (tempArr.OS == fingerprint.OS) {
                            temp++;
                        }
                        if (tempArr.osVersion == fingerprint.osVersion) {
                            temp++;
                        }
                        if (tempArr.isWindows == fingerprint.isWindows) {
                            temp++;
                        }
                        if (tempArr.isMac == fingerprint.isMac) {
                            temp++;
                        }
                        if (tempArr.isLinux == fingerprint.isLinux) {
                            temp++;
                        }
                        if (tempArr.isUbuntu == fingerprint.isUbuntu) {
                            temp++;
                        }
                        if (tempArr.isSolaris == fingerprint.isSolaris) {
                            temp++;
                        }
                        if (tempArr.device == fingerprint.device) {
                            temp++;
                        }
                        if (tempArr.deviceType == fingerprint.deviceType) {
                            temp++;
                        }
                        if (tempArr.deviceVendor == fingerprint.deviceVendor) {
                            temp++;
                        }
                        if (tempArr.CPU == fingerprint.CPU) {
                            temp++;
                        }
                        if (tempArr.isMobile == fingerprint.isMobile) {
                            temp++;
                        }
                        if (tempArr.isMobileMajor == fingerprint.isMobileMajor) {
                            temp++;
                        }
                        if (tempArr.isMobileAndroid == fingerprint.isMobileAndroid) {
                            temp++;
                        }
                        if (tempArr.isMobileOpera == fingerprint.isMobileOpera) {
                            temp++;
                        }
                        if (tempArr.isMobileWindows == fingerprint.isMobileWindows) {
                            temp++;
                        }
                        if (tempArr.isMobileBlackBerry == fingerprint.isMobileBlackBerry) {
                            temp++;
                        }
                        if (tempArr.isMobileIOS == fingerprint.isMobileIOS) {
                            temp++;
                        }
                        if (tempArr.isIphone == fingerprint.isIphone) {
                            temp++;
                        }
                        if (tempArr.isIpad == fingerprint.isIpad) {
                            temp++;
                        }
                        if (tempArr.isIpod == fingerprint.isIpod) {
                            temp++;
                        }
                        if (tempArr.screenPrint == fingerprint.screenPrint) {
                            temp++;
                        }
                        if (tempArr.colorDepth == fingerprint.colorDepth) {
                            temp++;
                        }
                        if (tempArr.currentResolution == fingerprint.currentResolution) {
                            temp++;
                        }
                        if (tempArr.availableResolution == fingerprint.availableResolution) {
                            temp++;
                        }
                        if (tempArr.deviceXDPI == fingerprint.deviceXDPI) {
                            temp++;
                        }
                        if (tempArr.deviceYDPI == fingerprint.deviceYDPI) {
                            temp++;
                        }
                        if (tempArr.plugins == fingerprint.plugins) {
                            temp++;
                        }
                        if (tempArr.isJava == fingerprint.isJava) {
                            temp++;
                        }
                        if (tempArr.javaVersion == fingerprint.javaVersion) {
                            temp++;
                        }
                        if (tempArr.isFlash == fingerprint.isFlash) {
                            temp++;
                        }
                        if (tempArr.flashVersion == fingerprint.flashVersion) {
                            temp++;
                        }
                        if (tempArr.isSilverlight == fingerprint.isSilverlight) {
                            temp++;
                        }
                        if (tempArr.silverlightVersion == fingerprint.silverlightVersion) {
                            temp++;
                        }
                        if (tempArr.isMimeTypes == fingerprint.isMimeTypes) {
                            temp++;
                        }
                        if (tempArr.mimeTypes == fingerprint.mimeTypes) {
                            temp++;
                        }
                        if (tempArr.isFont == fingerprint.isFont) {
                            temp++;
                        }
                        if (tempArr.fonts == fingerprint.fonts) {
                            temp++;
                        }
                        if (tempArr.isLocalStorage == fingerprint.isLocalStorage) {
                            temp++;
                        }
                        if (tempArr.isSessionStorage == fingerprint.isSessionStorage) {
                            temp++;
                        }
                        if (tempArr.isCookie == fingerprint.isCookie) {
                            temp++;
                        }
                        if (tempArr.timeZone == fingerprint.timeZone) {
                            temp++;
                        }
                        if (tempArr.language == fingerprint.language) {
                            temp++;
                        }
                        if (tempArr.isCanvas == fingerprint.isCanvas) {
                            temp++;
                        }
                        if (tempArr.canvasPrint == fingerprint.canvasPrint) {
                            temp++;
                        }
                        if (tempArr.platform == fingerprint.platform) {
                            temp++;
                        }
                        if (tempArr.browserLanguages == fingerprint.browserLanguages) {
                            temp++;
                        }
                        if (tempArr.hardwareConcurrency == fingerprint.hardwareConcurrency) {
                            temp++;
                        }
                        if (tempArr.product == fingerprint.product) {
                            temp++;
                        }
                        if (tempArr.productSub == fingerprint.productSub) {
                            temp++;
                        }
                        if (tempArr.browserName == fingerprint.browserName) {
                            temp++;
                        }
                        if (tempArr.screenWidth == fingerprint.screenWidth) {
                            temp++;
                        }
                        if (tempArr.screenHeight == fingerprint.screenHeight) {
                            temp++;
                        }
                        if (tempArr.availWidth == fingerprint.availWidth) {
                            temp++;
                        }
                        if (tempArr.availHeight == fingerprint.availHeight) {
                            temp++;
                        }
                        if (tempArr.colorDepth == fingerprint.colorDepth) {
                            temp++;
                        }
                        if (tempArr.pixelDepth == fingerprint.pixelDepth) {
                            temp++;
                        }

                        //Berechnung der prozentualen Übereinstimmung; insgesamt 70 Eigenschaften
                        var s = (temp / 70) * 100;
                        for (var x = 0; x < re.length; x++) {
                            res.push({ "result": re[x].search, "prob": s });
                        }
                    }
                    //Rekursion
                    work(res, arr, fingerprint).then(r => {
                        resolve(r);
                    })
                })
                .catch(er => {
                    console.log("Error find custom Result");
                    console.log(er);
                    reject(er);
                })
        }
    })

}

module.exports = compare;