var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//MongoDB Schema für den Google User der sich eingloggt
var googleSchema = new Schema({
    access_token: {
        required: true,
        type: String
    },
    refresh_token: {
        required: false,
        type: String
    },
    Name: {
        required: true,
        type: String
    },
    Vorname: {
        required: true,
        type: String
    },
    Nachname: {
        required: true,
        type: String
    },
    Google_ID: {
        required: true,
        type: String
    },
    Foto: {
        required: true,
        type: String
    },
    GPlus: {
        required: true,
        type: String
    }
});

var google = mongoose.model('google', googleSchema);

module.exports = google;
