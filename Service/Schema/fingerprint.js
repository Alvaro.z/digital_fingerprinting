var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// MongoDB Schema für den Fingerprint des Webseitenbesuchers
var fingerprintSchema = new Schema({
    /*ClientJS Methoden */
    fingerprint: {
        required: true,
        type: String
    },
    customFingerprint: {
        required: false,
        type: String
    },
    userAgent: {
        required: false,
        type: String
    },
    userAgentLowerCase: {
        required: false,
        type: String
    },
    browser: {
        required: false,
        type: String
    },
    browserVersion: {
        required: false,
        type: String
    },
    browserMajorVersion: {
        required: false,
        type: String
    },
    isIE: {
        required: false,
        type: String
    },
    isChrome: {
        required: false,
        type: String
    },
    isFirefox: {
        required: false,
        type: String
    },
    isSafari: {
        required: false,
        type: String
    },
    isOpera: {
        required: false,
        type: String
    },
    isMobileSafari: {
        required: false,
        type: String
    },
    engine: {
        required: false,
        type: String
    },
    engineVersion: {
        required: false,
        type: String
    },
    OS: {
        required: false,
        type: String
    },
    osVersion: {
        required: false,
        type: String
    },
    isWindows: {
        required: false,
        type: String
    },
    isMac: {
        required: false,
        type: String
    },
    isLinux: {
        required: false,
        type: String
    },
    isUbuntu: {
        required: false,
        type: String
    },
    isSolaris: {
        required: false,
        type: String
    },
    device: {
        required: false,
        type: String
    },
    deviceType: {
        required: false,
        type: String
    },
    deviceVendor: {
        required: false,
        type: String
    },
    CPU: {
        required: false,
        type: String
    },
    isMobile: {
        required: false,
        type: String
    },
    isMobileMajor: {
        required: false,
        type: String
    },
    isMobileAndroid: {
        required: false,
        type: String
    },
    isMobileOpera: {
        required: false,
        type: String
    },
    isMobileWindows: {
        required: false,
        type: String
    },
    isMobileBlackBerry: {
        required: false,
        type: String
    },
    isMobileIOS: {
        required: false,
        type: String
    },
    isIphone: {
        required: false,
        type: String
    },
    isIpad: {
        required: false,
        type: String
    },
    isIpod: {
        required: false,
        type: String
    },
    screenPrint: {
        required: false,
        type: String
    },
    colorDepth: {
        required: false,
        type: String
    },
    currentResolution: {
        required: false,
        type: String
    },
    availableResolution: {
        required: false,
        type: String
    },
    deviceXDPI: {
        required: false,
        type: String
    },
    deviceYDPI: {
        required: false,
        type: String
    },
    plugins: {
        required: false,
        type: String
    },
    isJava: {
        required: false,
        type: String
    },
    javaVersion: {
        required: false,
        type: String
    },
    isFlash: {
        required: false,
        type: String
    },
    flashVersion: {
        required: false,
        type: String
    },
    isSilverlight: {
        required: false,
        type: String
    },
    silverlightVersion: {
        required: false,
        type: String
    },
    isMimeTypes: {
        required: false,
        type: String
    },
    mimeTypes: {
        required: false,
        type: String
    },
    isFont: {
        required: false,
        type: String
    },
    fonts: {
        required: false,
        type: String
    },
    isLocalStorage: {
        required: false,
        type: String
    },
    isSessionStorage: {
        required: false,
        type: String
    },
    isCookie: {
        required: false,
        type: String
    },
    timeZone: {
        required: false,
        type: String
    },
    language: {
        required: false,
        type: String
    },
    isCanvas: {
        required: false,
        type: String
    },
    canvasPrint: {
        required: false,
        type: String
    },

    /*Normal Methods*/
    platform: {//navigator.platform
        required: false,
        type: String
    },
    doNotTrack: {//navigator.doNotTrack
        required: false,
        type: String
    },
    geolocation: {//navigator.geolocation
        required: false,
        type: Object
    },
    browserLanguages: {//navigator.languages
        required: false,
        type: Object
    },
    hardwareConcurrency: {//navigator.hardwareConcurrency
        required: false,
        type: String
    },
    product: {//navigator.product
        required: false,
        type: String
    },
    productSub: {//navigator.productSub
        required: false,
        type: String
    },
    browserName: {//navigator.appName
        required: false,
        type: String
    },
    screenWidth: {//window.screen.width 
        required: false,
        type: Object
    },
    screenHeight: {//window.screen.height 
        required: false,
        type: Object
    },
    availWidth: {//window.screen.availWidth 
        required: false,
        type: Object
    },
    availHeight: {//window.screen.availHeight
        required: false,
        type: Object
    },
    innerWidth: {//window.screen.innerWidth 
        required: false,
        type: Object
    },
    innerHeight: {//window.screen.innerHeight 
        required: false,
        type: Object
    },
    colorDepth: {//window.screen.colorDepth 
        required: false,
        type: Object
    },
    pixelDepth: {//window.screen.pixelDepth 
        required: false,
        type: Object
    },
});

var fingerprint = mongoose.model('fingerprint', fingerprintSchema);

module.exports = fingerprint;
