var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// MongoDB Schema für die Suchergebnisse
var resultSchema = new Schema({
    fingerprint: {
        required: true,
        type: String
    },
    search: {
        required: true,
        type: String
    },
});

var results = mongoose.model('results', resultSchema);

module.exports = results;
