var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var helmet = require('helmet');
var exphbs = require('express-handlebars');
var mongoose = require('mongoose');
var assert = require('assert');
var colors = require('colors');
var requestIp = require('request-ip');
var config = require('./config.json');

//Databases & Datastores --> Hier kann die Datenbank gewechselt werden
mongoose.connect(config.database, (err, db) => { err === null ? console.log("Connected to MongoDB".green) : console.log("Connection to MongoDB failed".red) });
mongoose.Promise = global.Promise;

//Routes
var index = require('./routes/index');
var data = require('./routes/data');
var auth = require('./routes/auth');

//Initialize App
var app = express();

// view engine setup
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Map Routes
app.use('/', index);
app.use('/data', data);
app.use('/auth', auth);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
