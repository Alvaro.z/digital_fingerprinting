// Fingerprint MEthoden aus ClientJS
var client = new ClientJS(); // Create A New Client Object

var fingerprint = client.getFingerprint(); // Get Client's Fingerprint

var ua = client.getBrowserData().ua;
var canvasPrint = client.getCanvasPrint();
var customFingerprint = client.getCustomFingerprint(ua, canvasPrint);

var userAgent = client.getUserAgent(); // Get User Agent String

var userAgentLowerCase = client.getUserAgentLowerCase(); // Get User Agent String

var browser = client.getBrowser(); // Get Browser

var browserVersion = client.getBrowserVersion(); // Get Browser Version

var browserMajorVersion = client.getBrowserMajorVersion(); // Get Browser's Major Version

var isIE = client.isIE(); // Check For IE

var isChrome = client.isChrome(); // Check For Chrome

var isFirefox = client.isFirefox(); // Check For Firefox

var isSafari = client.isSafari(); // Check For Safari

var isOpera = client.isOpera(); // Check For Opera

var isMobileSafari = client.isMobileSafari(); // Check For Mobile Safari

var engine = client.getEngine(); // Get Engine

var engineVersion = client.getEngineVersion(); // Get Engine Version

var OS = client.getOS(); // Get OS Version

var osVersion = client.getOSVersion(); // Get OS Version

var isWindows = client.isWindows(); // Check For Windows

var isMac = client.isMac(); // Check For Mac

var isLinux = client.isLinux(); // Check For Linux

var isUbuntu = client.isUbuntu(); // Check For Ubuntu

var isSolaris = client.isSolaris(); // Check For Solaris

var device = client.getDevice(); // Get Device

var deviceType = client.getDeviceType(); // Get Device Type

var deviceVendor = client.getDeviceVendor(); // Get Device Vendor

var CPU = client.getCPU(); // Get CPU Architecture

var isMobile = client.isMobile(); // Check For Mobile

var isMobileMajor = client.isMobileMajor(); // Check For Mobile Major

var isMobileAndroid = client.isMobileAndroid(); // Check For Mobile Android

var isMobileOpera = client.isMobileOpera(); // Check For Mobile Opera

var isMobileWindows = client.isMobileWindows(); // Check For Mobile Windows

var isMobileBlackBerry = client.isMobileBlackBerry(); // Check For Mobile Blackberry

var isMobileIOS = client.isMobileIOS(); // Check For Mobile iOS

var isIphone = client.isIphone(); // Check For iPhone

var isIpad = client.isIpad(); // Check For iPad

var isIpod = client.isIpod(); // Check For iPod

var screenPrint = client.getScreenPrint(); // Get Screen Print

var colorDepth = client.getColorDepth(); // Get Color Depth

var currentResolution = client.getCurrentResolution(); // Get Current Resolution

var availableResolution = client.getAvailableResolution(); // Get Available Resolution

var deviceXDPI = client.getDeviceXDPI(); // Get Device XDPI

var deviceYDPI = client.getDeviceYDPI(); // Get Device YDPI

var plugins = client.getPlugins(); // Get Plugins

var isJava = client.isJava(); // Check For Java

var javaVersion = client.getJavaVersion(); // Get Java Version

var isFlash = client.isFlash(); // Check For Flash

var flashVersion = client.getFlashVersion(); // GET Flash Version

var isSilverlight = client.isSilverlight(); // Check For Silverlight

var silverlightVersion = client.getSilverlightVersion(); // GET Silverlight Version

var isMimeTypes = client.isMimeTypes(); // Check For Mime Types

var mimeTypes = client.getMimeTypes(); // Get Mime Types

var font = "Times New Roman"; // Set Font String
var isFont = client.isFont(font); // Check For A Font

var fonts = client.getFonts(); // Get Fonts

var isLocalStorage = client.isLocalStorage(); // Check For Local Storage

var isSessionStorage = client.isSessionStorage(); // Check For Session Storage

var isCookie = client.isCookie(); // Check For Cookies

var timeZone = client.getTimeZone(); // Get Time Zone

var language = client.getLanguage(); // Get User Language

var isCanvas = client.isCanvas(); // Check For The Canvas Element

var canvasPrint = client.getCanvasPrint(); // Get Canvas Print

//AJAX Request welches bei Seitenaufruf gestartet wird und alle Daten ber den Fingerprint an das Backend sendet(/data) wo diese dann in die Datenbank gespeichert werden
$(function () {
    $.ajax({
        method: "POST",
        dataType: "json",
        url: "/data",
        data: {
            /*ClientJS Methoden*/
            "fingerprint": fingerprint,
            "customFingerprint": customFingerprint,
            "userAgent": userAgent,
            "userAgentLowerCase": userAgentLowerCase,
            "browser": browser,
            "browserVersion": browserVersion,
            "browserMajorVersion": browserMajorVersion,
            "isIE": isIE,
            "isChrome": isChrome,
            "isFirefox": isFirefox,
            "isSafari": isSafari,
            "isOpera": isOpera,
            "isMobileSafari": isMobileSafari,
            "engine": engine,
            "engineVersion": engineVersion,
            "OS": OS,
            "osVersion": osVersion,
            "isWindows": isWindows,
            "isMac": isMac,
            "isLinux": isLinux,
            "isUbuntu": isUbuntu,
            "isSolaris": isSolaris,
            "device": device,
            "deviceType": deviceType,
            "deviceVendor": deviceVendor,
            "CPU": CPU,
            "isMobile": isMobile,
            "isMobileMajor": isMobileMajor,
            "isMobileAndroid": isMobileAndroid,
            "isMobileOpera": isMobileOpera,
            "isMobileWindows": isMobileWindows,
            "isMobileBlackBerry": isMobileBlackBerry,
            "isMobileIOS": isMobileIOS,
            "isIphone": isIphone,
            "isIpad": isIpad,
            "isIpod": isIpod,
            "screenPrint": screenPrint,
            "colorDepth": colorDepth,
            "currentResolution": currentResolution,
            "availableResolution": availableResolution,
            "deviceXDPI": deviceXDPI,
            "deviceYDPI": deviceYDPI,
            "plugins": plugins,
            "isJava": isJava,
            "javaVersion": javaVersion,
            "isFlash": isFlash,
            "flashVersion": flashVersion,
            "isSilverlight": isSilverlight,
            "silverlightVersion": silverlightVersion,
            "isMimeTypes": isMimeTypes,
            "mimeTypes": mimeTypes,
            "isFont": isFont,
            "fonts": fonts,
            "isLocalStorage": isLocalStorage,
            "isSessionStorage": isSessionStorage,
            "isCookie": isCookie,
            "timeZone": timeZone,
            "language": language,
            "isCanvas": isCanvas,
            "canvasPrint": canvasPrint,
            /*Normal Methoden*/
            "platform": navigator.platform,
            "browserLanguages": navigator.languages,
            "hardwareConcurrency": navigator.hardwareConcurrency,
            "product": navigator.product,
            "productSub": navigator.productSub,
            "browserName": navigator.appName,
            "screenWidth": window.screen.width,
            "screenHeight": window.screen.height,
            "availWidth": window.screen.availWidth,
            "availHeight": window.screen.availHeight,
            "innerWidth": window.screen.innerWidth,
            "innerHeight": window.screen.innerHeight,
            "colorDepth": window.screen.colorDepth,
            "pixelDepth": window.screen.pixelDepth
        }
    })
        // Wenn das Senden an das Backend erfolgreich war wird eine Tabelle erzeugt mit allen Suchergebnissen und deren Übereinstimmungswahrscheinlichenkeiten zu dem identifizierten Fingerprint
        .done(function (msg) {
            document.getElementById("preload").style = "display: none";
            var r = document.getElementById("suchergebnisse").appendChild(document.createElement("tr"));
            r.appendChild(document.createElement("th")).innerHTML = "Suchergebnis";
            r.appendChild(document.createElement("th")).innerHTML = "Wahrscheinlichkeit";
            for (var i = 0; i < msg.length; i++) {
                var r = document.getElementById("suchergebnisse").appendChild(document.createElement("tr"));
                r.appendChild(document.createElement("td")).innerHTML = msg[i].result;
                r.appendChild(document.createElement("td")).innerHTML = msg[i].prob;
            }
        });
})

//Abbildung des identifizierten Fingerprints auf dem index.hanelbars View = Zeile 3 bei <a id="fingerprint"></a>
document.getElementById('fingerprint').innerHTML = "Digital fingerprint: " + customFingerprint;
document.getElementsByName('fingerprintID')[0].value = customFingerprint;